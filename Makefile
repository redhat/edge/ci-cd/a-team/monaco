
.PHONY: run
run: reqs
	ansible-playbook ./deploy/playbooks/run.yml

.PHONY: reqs
reqs:
	ansible-galaxy collection install community.kubernetes community.hashi_vault
	pip3 install hvac openshift
	